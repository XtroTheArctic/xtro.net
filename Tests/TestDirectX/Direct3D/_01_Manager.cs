﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Xtro.DirectX.Direct3D;
using Xtro.Gerecler;

namespace TestDirectX.Direct3D
{
    [TestClass]
// ReSharper disable InconsistentNaming
    public class _01_Manager
// ReSharper restore InconsistentNaming
    {
        [TestMethod]
        public void StartStop()
        {
            // Stop method must throw EInvalidCall when the Manager is already inactive.
            try
            {
                Manager.Stop();
                Assert.Fail();
            }
            catch (EInvalidCall) { }

            // Testing the initial member values
            Assert.IsTrue(Manager.Adapters.Count == 0);
            Assert.IsFalse(Manager.Active);

            // Starting the Manager
            Manager.Start();

            // Start method must throw EInvalidCall when the Manager is already active. 
            try
            {
                Manager.Start();
                Assert.Fail();
            }
            catch (EInvalidCall) { }

            // Testing the members while in action
            Assert.IsTrue(Manager.Adapters.Count > 0);
            Assert.IsTrue(Manager.Active);

            // Stopping the Manager
            Manager.Stop();

            // Stop method must throw EInvalidCall when the Manager is already inactive.
            try
            {
                Manager.Stop();
                Assert.Fail();
            }
            catch (EInvalidCall) { }

            // Testing the post values 
            Assert.IsTrue(Manager.Adapters.Count == 0);
            Assert.IsFalse(Manager.Active);
        }
    }
}
