﻿using System.Collections.Generic;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Xtro.DirectX.Direct3D;
using Xtro.Gerecler;
using Xtro.MDX;
using Xtro.MDX.Direct3DX10;
using Color = Xtro.MDX.Direct3DX10.Color;

namespace TestDirectX.Direct3D
{
    [TestClass]
// ReSharper disable InconsistentNaming
    public class _02_Context
// ReSharper restore InconsistentNaming
    {
        [TestMethod]
        public void _01_StartStop()
        {
            // Creating a TContext must throw EInvalidCall when the Manager is not active.
            try { new TContext("test context"); Assert.Fail(); }
            catch (EInvalidCall) { }

            Manager.Start();
            var Context = new TContext("test context");

            Context.Render += Context_Render;

            // Stop method must throw EInvalidCall when the context is not active.
            try { Context.Stop(); Assert.Fail(); }
            catch (EInvalidCall) { }

            // Testing the initial member values
            Assert.IsFalse(Context.Active);

            // PerformRender method must throw EInvalidCall when the context is not active.
            try { Context.PerformRender(); Assert.Fail(); }
            catch (EInvalidCall) { }

            // Starting
            Context.Start();

            // Testing the members while in action
            Assert.IsTrue(Context.Active);

            Context.PerformRender();
                                                  
            // Stopping
            Context.Stop();

            // Testing the post values 
            Assert.IsFalse(Context.Active);

            // PerformRender method must throw EInvalidCall when the context is not active.
            try { Context.PerformRender(); Assert.Fail(); }
            catch (EInvalidCall) { }

            // Stop method must throw EInvalidCall when the context is not active.
            try { Context.Stop(); Assert.Fail(); }
            catch (EInvalidCall) { }

            Manager.Stop();
        }

        [TestMethod]
        public void _02_DrawText()
        {
            Manager.Start();
            var Context = new TContext("test context");

            Context.Render += Context_Render;

            // Methods must throw EInvalidCall when the context is not active.

            try { Context.BeginDrawText(); Assert.Fail(); }
            catch (EInvalidCall E)
            {
                if (E.Reason != Xtro.DirectX.ResourcesReason.NotActive) Assert.Fail();
            }

            try { Context.EndDrawText(); Assert.Fail(); }
            catch (EInvalidCall E)
            {
                if (E.Reason != Xtro.DirectX.ResourcesReason.NotActive) Assert.Fail();
            }

            var Box = new Rectangle();
            var Color = new Color();
            try { Context.DrawText(ref Box, "test test", ref Color, 0); Assert.Fail(); }
            catch (EInvalidCall) { }

            var FontDescription = new FontDescription();
            Context.SetFont(ref FontDescription);

            // Starting
            Context.Start();

            // Testing the members while in action
            Assert.IsTrue(Context.Active);

            FontDescription = new FontDescription { CharacterSet = FontCharacterSet.Turkish };
            Context.SetFont(ref FontDescription);

            // EndDrawText method must throw EInvalidCall when it is called before BeginDrawText.
            try { Context.EndDrawText(); Assert.Fail(); }
            catch (EInvalidCall E)
            {
                if (E.Reason != Xtro.DirectX.ResourcesReason.TextDrawingHasNotBegun) Assert.Fail();
            }

            Context.BeginDrawText();

            // BeginDrawText method must throw EInvalidCall when it is called after BeginDrawText.
            try { Context.BeginDrawText(); Assert.Fail(); }
            catch (EInvalidCall E)
            {
                if (E.Reason != Xtro.DirectX.ResourcesReason.TextDrawingHasAlreadyBegun) Assert.Fail();
            }

            Context.DrawText(ref Box, "test test", ref Color, 0);

            Context.EndDrawText();

            Context.DrawText(ref Box, "test test", ref Color, 0);
            Context.PerformRender();
            Context.SetFont(ref FontDescription);

            // Stopping
            Context.Stop();

            // Testing the post values 
            Assert.IsFalse(Context.Active);

            // Methods must throw EInvalidCall when the context is not active.

            try { Context.DrawText(ref Box, "test test", ref Color, 0); Assert.Fail(); }
            catch (EInvalidCall) { }

            Manager.Stop();
        }

        static void Context_Render(TContext Sender, List<TScreen> Screens)
        {            
        }
    }
}
