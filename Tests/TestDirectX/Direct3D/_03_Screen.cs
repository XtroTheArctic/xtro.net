﻿using System.Drawing;
using System.Windows.Forms;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Xtro.DirectX.Direct3D;
using Xtro.Gerecler;
using Color = Xtro.MDX.Direct3DX10.Color;

namespace TestDirectX.Direct3D
{
    [TestClass]
// ReSharper disable InconsistentNaming
    public class _03_Screen
// ReSharper restore InconsistentNaming
    {
        [TestMethod]
        public void _01_CreateDelete()
        {
            Manager.Start();
            var Context = new TContext("test context");

            // Null parameters
            
            try { new TScreen("test screen",null,new Panel(),Size.Empty,0); Assert.Fail(); }
            catch (EInvalidParameter) { }

            try { new TScreen("test screen", Context, null, Size.Empty, 0); Assert.Fail(); }
            catch (EInvalidParameter) { }

            var Screen = new TScreen("test screen", Context, new Panel(), Size.Empty, 0);

            var Color = new Color();

            try { Screen.DrawFps(0, 0, null, ref Color); Assert.Fail(); }
            catch (EInvalidCall) { }

            Context.Start();
            Screen.DrawFps(0, 0, null, ref Color);

            Screen.Delete();

            try { Screen.DrawFps(0, 0, null, ref Color); Assert.Fail(); }
            catch (EInvalidCall) { }

            try { Screen.Delete(); Assert.Fail(); }
            catch (EInvalidCall) { }

            Screen = new TScreen("test screen", Context, new Panel(), Size.Empty, 0);
            Screen.DrawFps(0, 0, null, ref Color);
            Context.Stop();

            try { Screen.DrawFps(0, 0, null, ref Color); Assert.Fail(); }
            catch (EInvalidCall) { }

            Manager.Stop();
        }
    }
}
