﻿using System;
using System.Windows.Forms;
using Xtro.MDX.DXGI;
using MessageBox = Xtro.Gerecler.MessageBox;

namespace XtroDirectXDemo
{
    // ReSharper disable InconsistentNaming
    static class Program
    // ReSharper restore InconsistentNaming
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try { TestDirectX(); }
            catch
            {
                MessageBox.Show("Xtro.MDX can not be loaded.\n\nThese libraries should be installed :\n - VC++ 2010 SP1 runtime (x86)\n - DirectX 10 June 2010 redistrubutable runtime.", "Load Error", 0);

                return;
            }

            RunApplication();
        }

        static void TestDirectX()
        {
            Factory Factory;
            Functions.CreateFactory(null, out Factory);
        }

        static void RunApplication()
        {
            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FormMain());
        }
    }
}