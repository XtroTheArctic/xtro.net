﻿namespace XtroDirectXDemo
{
    internal sealed partial class FormRenderTarget
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // FormRenderTarget
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(0)))), ((int)(((byte)(153)))));
            this.ClientSize = new System.Drawing.Size(434, 349);
            this.KeyPreview = true;
            this.Name = "FormRenderTarget";
            this.Text = "Render Window";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormRenderTarget_FormClosing);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.FormRenderTarget_Paint);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormRenderTarget_KeyDown);
            this.Resize += new System.EventHandler(this.FormRenderTarget_Resize);
            this.ResumeLayout(false);

        }

        #endregion

    }
}