﻿using System;
using System.Windows.Forms;
using FormsMessage = System.Windows.Forms.Message;

namespace XtroDirectXDemo
{
    // ReSharper disable InconsistentNaming
    internal sealed partial class FormRenderTarget : Form
    // ReSharper restore InconsistentNaming
    {
        readonly FormMain FormMain;

        public FormRenderTarget(FormMain FormMain)
        {
            this.FormMain = FormMain;

            InitializeComponent();
        }

        private void FormRenderTarget_FormClosing(object Sender, FormClosingEventArgs E)
        {
            E.Cancel = E.CloseReason == CloseReason.UserClosing;
        }

        private void FormRenderTarget_Paint(object Sender, PaintEventArgs E)
        {
            if (!FormMain.CheckBoxLooping.Checked) return;

            if (!FormMain.CheckBoxLiveWindow.Checked && FormMain.Context != null && FormMain.Context.Active && FormMain.ScreenWindow != null) FormMain.Context.PerformRender(FormMain.ScreenWindow);
        }

        private void FormRenderTarget_Resize(object Sender, EventArgs E)
        {
            if (!FormMain.CheckBoxLiveWindow.Checked) Invalidate();
        }

        protected override bool ProcessKeyEventArgs(ref FormsMessage Message)
        {
            if ((Keys)Message.WParam == Keys.PrintScreen) FormMain.ScreenShot();

            return base.ProcessKeyEventArgs(ref Message);
        }

        private void FormRenderTarget_KeyDown(object Sender, KeyEventArgs E)
        {
            if (FormMain.FormSetup.Visible) return;

            switch (E.KeyCode)
            {
            case Keys.O:
                FormMain.CheckBoxLooping.Checked = !FormMain.CheckBoxLooping.Checked;
                break;
            case Keys.R:
                FormMain.CheckBoxAutoResize.Checked = !FormMain.CheckBoxAutoResize.Checked;
                break;
            case Keys.D1:
                FormMain.CheckBoxLivePanel1.Checked = !FormMain.CheckBoxLivePanel1.Checked;
                break;
            case Keys.D2:
                FormMain.CheckBoxLivePanel2.Checked = !FormMain.CheckBoxLivePanel2.Checked;
                break;
            case Keys.D3:
                FormMain.CheckBoxLiveWindow.Checked = !FormMain.CheckBoxLiveWindow.Checked;
                break;
            case Keys.E:
                FormMain.ButtonEnableAll.PerformClick();
                break;
            case Keys.D:
                FormMain.ButtonDisableAll.PerformClick();
                break;
            default:
                FormMain.FormMain_KeyDown(Sender, E);
                break;
            }
        }
    }
}