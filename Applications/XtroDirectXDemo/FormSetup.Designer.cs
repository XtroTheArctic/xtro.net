﻿namespace XtroDirectXDemo
{
    internal sealed partial class FormSetup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormSetup));
            this.ButtonOK = new System.Windows.Forms.Button();
            this.ButtonCancel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.ComboBoxAdapter = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.ComboBoxMonitor = new System.Windows.Forms.ComboBox();
            this.GroupBoxResolution = new System.Windows.Forms.GroupBox();
            this.CheckBoxAllAspectRatios = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.ListBoxFrequency = new System.Windows.Forms.ListBox();
            this.ListBoxResolution = new System.Windows.Forms.ListBox();
            this.CheckBoxFullscreen = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ComboBoxAntialiasing = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.CheckBoxVerticalSync = new System.Windows.Forms.CheckBox();
            this.CheckBoxDebugLayer = new System.Windows.Forms.CheckBox();
            this.CheckBoxAutoStart = new System.Windows.Forms.CheckBox();
            this.GroupBoxDevice = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.CheckBoxRenderTarget1 = new System.Windows.Forms.CheckBox();
            this.RadioButtonRenderTarget1 = new System.Windows.Forms.RadioButton();
            this.CheckBoxRenderTarget2 = new System.Windows.Forms.CheckBox();
            this.RadioButtonRenderTarget2 = new System.Windows.Forms.RadioButton();
            this.CheckBoxRenderTarget3 = new System.Windows.Forms.CheckBox();
            this.RadioButtonRenderTarget3 = new System.Windows.Forms.RadioButton();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.GroupBoxResolution.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.GroupBoxDevice.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ButtonOK
            // 
            this.ButtonOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.ButtonOK.Image = ((System.Drawing.Image)(resources.GetObject("ButtonOK.Image")));
            this.ButtonOK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonOK.Location = new System.Drawing.Point(176, 357);
            this.ButtonOK.Name = "ButtonOK";
            this.ButtonOK.Size = new System.Drawing.Size(63, 47);
            this.ButtonOK.TabIndex = 0;
            this.ButtonOK.Text = "&OK   ";
            this.ButtonOK.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ButtonOK.UseVisualStyleBackColor = true;
            // 
            // ButtonCancel
            // 
            this.ButtonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.ButtonCancel.Location = new System.Drawing.Point(245, 357);
            this.ButtonCancel.Name = "ButtonCancel";
            this.ButtonCancel.Size = new System.Drawing.Size(63, 47);
            this.ButtonCancel.TabIndex = 1;
            this.ButtonCancel.Text = "&Cancel";
            this.ButtonCancel.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Adapter :";
            // 
            // ComboBoxAdapter
            // 
            this.ComboBoxAdapter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ComboBoxAdapter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxAdapter.FormattingEnabled = true;
            this.ComboBoxAdapter.Location = new System.Drawing.Point(62, 13);
            this.ComboBoxAdapter.Name = "ComboBoxAdapter";
            this.ComboBoxAdapter.Size = new System.Drawing.Size(228, 21);
            this.ComboBoxAdapter.TabIndex = 2;
            this.ComboBoxAdapter.SelectedIndexChanged += new System.EventHandler(this.ComboBoxAdapter_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Monitor :";
            // 
            // ComboBoxMonitor
            // 
            this.ComboBoxMonitor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxMonitor.DropDownWidth = 200;
            this.ComboBoxMonitor.FormattingEnabled = true;
            this.ComboBoxMonitor.Location = new System.Drawing.Point(60, 19);
            this.ComboBoxMonitor.Name = "ComboBoxMonitor";
            this.ComboBoxMonitor.Size = new System.Drawing.Size(110, 21);
            this.ComboBoxMonitor.TabIndex = 3;
            this.ComboBoxMonitor.SelectedIndexChanged += new System.EventHandler(this.ComboBoxMonitor_SelectedIndexChanged);
            // 
            // GroupBoxResolution
            // 
            this.GroupBoxResolution.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GroupBoxResolution.Controls.Add(this.CheckBoxAllAspectRatios);
            this.GroupBoxResolution.Controls.Add(this.label4);
            this.GroupBoxResolution.Controls.Add(this.label3);
            this.GroupBoxResolution.Controls.Add(this.ListBoxFrequency);
            this.GroupBoxResolution.Controls.Add(this.ListBoxResolution);
            this.GroupBoxResolution.Controls.Add(this.ComboBoxMonitor);
            this.GroupBoxResolution.Controls.Add(this.label2);
            this.GroupBoxResolution.Location = new System.Drawing.Point(12, 197);
            this.GroupBoxResolution.Name = "GroupBoxResolution";
            this.GroupBoxResolution.Size = new System.Drawing.Size(296, 153);
            this.GroupBoxResolution.TabIndex = 6;
            this.GroupBoxResolution.TabStop = false;
            // 
            // CheckBoxAllAspectRatios
            // 
            this.CheckBoxAllAspectRatios.AutoSize = true;
            this.CheckBoxAllAspectRatios.Location = new System.Drawing.Point(187, 21);
            this.CheckBoxAllAspectRatios.Name = "CheckBoxAllAspectRatios";
            this.CheckBoxAllAspectRatios.Size = new System.Drawing.Size(100, 17);
            this.CheckBoxAllAspectRatios.TabIndex = 5;
            this.CheckBoxAllAspectRatios.Text = "All aspect &ratios";
            this.CheckBoxAllAspectRatios.UseVisualStyleBackColor = true;
            this.CheckBoxAllAspectRatios.CheckedChanged += new System.EventHandler(this.CheckBoxAllAspectRatios_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(210, 43);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Frequency :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 43);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Resolution :";
            // 
            // ListBoxFrequency
            // 
            this.ListBoxFrequency.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.ListBoxFrequency.FormattingEnabled = true;
            this.ListBoxFrequency.Location = new System.Drawing.Point(213, 59);
            this.ListBoxFrequency.Name = "ListBoxFrequency";
            this.ListBoxFrequency.Size = new System.Drawing.Size(73, 82);
            this.ListBoxFrequency.TabIndex = 2;
            this.ListBoxFrequency.SelectedIndexChanged += new System.EventHandler(this.ListBoxFrequency_SelectedIndexChanged);
            // 
            // ListBoxResolution
            // 
            this.ListBoxResolution.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.ListBoxResolution.ColumnWidth = 65;
            this.ListBoxResolution.FormattingEnabled = true;
            this.ListBoxResolution.Location = new System.Drawing.Point(6, 59);
            this.ListBoxResolution.MultiColumn = true;
            this.ListBoxResolution.Name = "ListBoxResolution";
            this.ListBoxResolution.Size = new System.Drawing.Size(200, 82);
            this.ListBoxResolution.TabIndex = 1;
            this.ListBoxResolution.SelectedIndexChanged += new System.EventHandler(this.ListBoxResolution_SelectedIndexChanged);
            // 
            // CheckBoxFullscreen
            // 
            this.CheckBoxFullscreen.AutoSize = true;
            this.CheckBoxFullscreen.Location = new System.Drawing.Point(21, 197);
            this.CheckBoxFullscreen.Name = "CheckBoxFullscreen";
            this.CheckBoxFullscreen.Size = new System.Drawing.Size(74, 17);
            this.CheckBoxFullscreen.TabIndex = 4;
            this.CheckBoxFullscreen.Text = "&Fullscreen";
            this.CheckBoxFullscreen.UseVisualStyleBackColor = true;
            this.CheckBoxFullscreen.CheckedChanged += new System.EventHandler(this.CheckBoxFullscreen_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.ComboBoxAntialiasing);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Location = new System.Drawing.Point(12, 151);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(296, 40);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Options";
            // 
            // ComboBoxAntialiasing
            // 
            this.ComboBoxAntialiasing.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxAntialiasing.FormattingEnabled = true;
            this.ComboBoxAntialiasing.Location = new System.Drawing.Point(81, 13);
            this.ComboBoxAntialiasing.Name = "ComboBoxAntialiasing";
            this.ComboBoxAntialiasing.Size = new System.Drawing.Size(89, 21);
            this.ComboBoxAntialiasing.TabIndex = 0;
            this.ComboBoxAntialiasing.SelectedIndexChanged += new System.EventHandler(this.ComboBoxAntialiasing_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Anti-aliasing :";
            // 
            // CheckBoxVerticalSync
            // 
            this.CheckBoxVerticalSync.AutoSize = true;
            this.CheckBoxVerticalSync.Location = new System.Drawing.Point(204, 40);
            this.CheckBoxVerticalSync.Name = "CheckBoxVerticalSync";
            this.CheckBoxVerticalSync.Size = new System.Drawing.Size(86, 17);
            this.CheckBoxVerticalSync.TabIndex = 2;
            this.CheckBoxVerticalSync.Text = "&Vertical sync";
            this.CheckBoxVerticalSync.UseVisualStyleBackColor = true;
            // 
            // CheckBoxDebugLayer
            // 
            this.CheckBoxDebugLayer.AutoSize = true;
            this.CheckBoxDebugLayer.Location = new System.Drawing.Point(9, 40);
            this.CheckBoxDebugLayer.Name = "CheckBoxDebugLayer";
            this.CheckBoxDebugLayer.Size = new System.Drawing.Size(191, 17);
            this.CheckBoxDebugLayer.TabIndex = 1;
            this.CheckBoxDebugLayer.Text = "&Debug layer (DirectX SDK needed)";
            this.CheckBoxDebugLayer.UseVisualStyleBackColor = true;
            // 
            // CheckBoxAutoStart
            // 
            this.CheckBoxAutoStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.CheckBoxAutoStart.AutoSize = true;
            this.CheckBoxAutoStart.Checked = true;
            this.CheckBoxAutoStart.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CheckBoxAutoStart.Location = new System.Drawing.Point(21, 373);
            this.CheckBoxAutoStart.Name = "CheckBoxAutoStart";
            this.CheckBoxAutoStart.Size = new System.Drawing.Size(71, 17);
            this.CheckBoxAutoStart.TabIndex = 8;
            this.CheckBoxAutoStart.Text = "&Auto start";
            this.CheckBoxAutoStart.UseVisualStyleBackColor = true;
            // 
            // GroupBoxDevice
            // 
            this.GroupBoxDevice.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GroupBoxDevice.Controls.Add(this.CheckBoxVerticalSync);
            this.GroupBoxDevice.Controls.Add(this.label1);
            this.GroupBoxDevice.Controls.Add(this.CheckBoxDebugLayer);
            this.GroupBoxDevice.Controls.Add(this.ComboBoxAdapter);
            this.GroupBoxDevice.Location = new System.Drawing.Point(12, 12);
            this.GroupBoxDevice.Name = "GroupBoxDevice";
            this.GroupBoxDevice.Size = new System.Drawing.Size(296, 65);
            this.GroupBoxDevice.TabIndex = 9;
            this.GroupBoxDevice.TabStop = false;
            this.GroupBoxDevice.Text = "Device";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tableLayoutPanel1);
            this.groupBox2.Location = new System.Drawing.Point(12, 83);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(296, 62);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Render targets";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Controls.Add(this.CheckBoxRenderTarget1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.RadioButtonRenderTarget1, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.CheckBoxRenderTarget2, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.RadioButtonRenderTarget2, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.CheckBoxRenderTarget3, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.RadioButtonRenderTarget3, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(290, 43);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // CheckBoxRenderTarget1
            // 
            this.CheckBoxRenderTarget1.AutoSize = true;
            this.CheckBoxRenderTarget1.Location = new System.Drawing.Point(75, 3);
            this.CheckBoxRenderTarget1.Name = "CheckBoxRenderTarget1";
            this.CheckBoxRenderTarget1.Size = new System.Drawing.Size(62, 17);
            this.CheckBoxRenderTarget1.TabIndex = 2;
            this.CheckBoxRenderTarget1.Text = "Panel 1";
            this.CheckBoxRenderTarget1.UseVisualStyleBackColor = true;
            this.CheckBoxRenderTarget1.CheckedChanged += new System.EventHandler(this.CheckBoxRenderTarget_CheckedChanged);
            // 
            // RadioButtonRenderTarget1
            // 
            this.RadioButtonRenderTarget1.AutoSize = true;
            this.RadioButtonRenderTarget1.Location = new System.Drawing.Point(75, 26);
            this.RadioButtonRenderTarget1.Name = "RadioButtonRenderTarget1";
            this.RadioButtonRenderTarget1.Size = new System.Drawing.Size(14, 13);
            this.RadioButtonRenderTarget1.TabIndex = 3;
            this.RadioButtonRenderTarget1.UseVisualStyleBackColor = true;
            this.RadioButtonRenderTarget1.CheckedChanged += new System.EventHandler(this.RadioButtonRenderTarget_CheckedChanged);
            // 
            // CheckBoxRenderTarget2
            // 
            this.CheckBoxRenderTarget2.AutoSize = true;
            this.CheckBoxRenderTarget2.Location = new System.Drawing.Point(147, 3);
            this.CheckBoxRenderTarget2.Name = "CheckBoxRenderTarget2";
            this.CheckBoxRenderTarget2.Size = new System.Drawing.Size(62, 17);
            this.CheckBoxRenderTarget2.TabIndex = 4;
            this.CheckBoxRenderTarget2.Text = "Panel 2";
            this.CheckBoxRenderTarget2.UseVisualStyleBackColor = true;
            this.CheckBoxRenderTarget2.CheckedChanged += new System.EventHandler(this.CheckBoxRenderTarget_CheckedChanged);
            // 
            // RadioButtonRenderTarget2
            // 
            this.RadioButtonRenderTarget2.AutoSize = true;
            this.RadioButtonRenderTarget2.Location = new System.Drawing.Point(147, 26);
            this.RadioButtonRenderTarget2.Name = "RadioButtonRenderTarget2";
            this.RadioButtonRenderTarget2.Size = new System.Drawing.Size(14, 13);
            this.RadioButtonRenderTarget2.TabIndex = 5;
            this.RadioButtonRenderTarget2.UseVisualStyleBackColor = true;
            this.RadioButtonRenderTarget2.CheckedChanged += new System.EventHandler(this.RadioButtonRenderTarget_CheckedChanged);
            // 
            // CheckBoxRenderTarget3
            // 
            this.CheckBoxRenderTarget3.AutoSize = true;
            this.CheckBoxRenderTarget3.Location = new System.Drawing.Point(219, 3);
            this.CheckBoxRenderTarget3.Name = "CheckBoxRenderTarget3";
            this.CheckBoxRenderTarget3.Size = new System.Drawing.Size(65, 17);
            this.CheckBoxRenderTarget3.TabIndex = 6;
            this.CheckBoxRenderTarget3.Text = "Window";
            this.CheckBoxRenderTarget3.UseVisualStyleBackColor = true;
            this.CheckBoxRenderTarget3.CheckedChanged += new System.EventHandler(this.CheckBoxRenderTarget_CheckedChanged);
            // 
            // RadioButtonRenderTarget3
            // 
            this.RadioButtonRenderTarget3.AutoSize = true;
            this.RadioButtonRenderTarget3.Location = new System.Drawing.Point(219, 26);
            this.RadioButtonRenderTarget3.Name = "RadioButtonRenderTarget3";
            this.RadioButtonRenderTarget3.Size = new System.Drawing.Size(14, 13);
            this.RadioButtonRenderTarget3.TabIndex = 7;
            this.RadioButtonRenderTarget3.UseVisualStyleBackColor = true;
            this.RadioButtonRenderTarget3.CheckedChanged += new System.EventHandler(this.RadioButtonRenderTarget_CheckedChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 3);
            this.label6.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Enabled :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 26);
            this.label7.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Settings :";
            // 
            // FormSetup
            // 
            this.AcceptButton = this.ButtonOK;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.CancelButton = this.ButtonCancel;
            this.ClientSize = new System.Drawing.Size(320, 416);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.GroupBoxDevice);
            this.Controls.Add(this.CheckBoxAutoStart);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.CheckBoxFullscreen);
            this.Controls.Add(this.GroupBoxResolution);
            this.Controls.Add(this.ButtonCancel);
            this.Controls.Add(this.ButtonOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormSetup";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Setup";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormSetup_FormClosing);
            this.Shown += new System.EventHandler(this.FormSetup_Shown);
            this.GroupBoxResolution.ResumeLayout(false);
            this.GroupBoxResolution.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.GroupBoxDevice.ResumeLayout(false);
            this.GroupBoxDevice.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ButtonOK;
        private System.Windows.Forms.Button ButtonCancel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox ComboBoxMonitor;
        private System.Windows.Forms.GroupBox GroupBoxResolution;
        private System.Windows.Forms.CheckBox CheckBoxFullscreen;
        private System.Windows.Forms.ListBox ListBoxResolution;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox ListBoxFrequency;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox ComboBoxAntialiasing;
        private System.Windows.Forms.CheckBox CheckBoxAllAspectRatios;
        public System.Windows.Forms.CheckBox CheckBoxDebugLayer;
        public System.Windows.Forms.CheckBox CheckBoxAutoStart;
        private System.Windows.Forms.GroupBox GroupBoxDevice;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.RadioButton RadioButtonRenderTarget1;
        private System.Windows.Forms.RadioButton RadioButtonRenderTarget2;
        public System.Windows.Forms.CheckBox CheckBoxVerticalSync;
        public System.Windows.Forms.ComboBox ComboBoxAdapter;
        public System.Windows.Forms.CheckBox CheckBoxRenderTarget1;
        public System.Windows.Forms.CheckBox CheckBoxRenderTarget2;
        public System.Windows.Forms.CheckBox CheckBoxRenderTarget3;
        public System.Windows.Forms.RadioButton RadioButtonRenderTarget3;
    }
}