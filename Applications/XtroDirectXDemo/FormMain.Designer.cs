﻿namespace XtroDirectXDemo
{
    sealed partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.CheckBoxLooping = new System.Windows.Forms.CheckBox();
            this.PanelRenderTarget1 = new System.Windows.Forms.Panel();
            this.ButtonStartStop = new System.Windows.Forms.Button();
            this.TimerRun = new System.Windows.Forms.Timer(this.components);
            this.Panel1 = new System.Windows.Forms.Panel();
            this.CheckBoxAutoResize = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ButtonEnableAll = new System.Windows.Forms.Button();
            this.ButtonDisableAll = new System.Windows.Forms.Button();
            this.CheckBoxLiveWindow = new System.Windows.Forms.CheckBox();
            this.CheckBoxLivePanel2 = new System.Windows.Forms.CheckBox();
            this.CheckBoxLivePanel1 = new System.Windows.Forms.CheckBox();
            this.PanelRenderTarget2 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.Panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // CheckBoxLooping
            // 
            this.CheckBoxLooping.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CheckBoxLooping.AutoSize = true;
            this.CheckBoxLooping.Checked = true;
            this.CheckBoxLooping.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CheckBoxLooping.Location = new System.Drawing.Point(12, 427);
            this.CheckBoxLooping.Name = "CheckBoxLooping";
            this.CheckBoxLooping.Size = new System.Drawing.Size(105, 17);
            this.CheckBoxLooping.TabIndex = 1;
            this.CheckBoxLooping.Text = "Application L&oop";
            this.CheckBoxLooping.UseVisualStyleBackColor = true;
            // 
            // PanelRenderTarget1
            // 
            this.PanelRenderTarget1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(0)))), ((int)(((byte)(153)))));
            this.PanelRenderTarget1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelRenderTarget1.Location = new System.Drawing.Point(0, 0);
            this.PanelRenderTarget1.Margin = new System.Windows.Forms.Padding(0);
            this.PanelRenderTarget1.Name = "PanelRenderTarget1";
            this.PanelRenderTarget1.Size = new System.Drawing.Size(538, 456);
            this.PanelRenderTarget1.TabIndex = 2;
            this.PanelRenderTarget1.Paint += new System.Windows.Forms.PaintEventHandler(this.PanelRenderTarget1_Paint);
            this.PanelRenderTarget1.Resize += new System.EventHandler(this.PanelRenderTarget1_Resize);
            // 
            // ButtonStartStop
            // 
            this.ButtonStartStop.Location = new System.Drawing.Point(12, 12);
            this.ButtonStartStop.Name = "ButtonStartStop";
            this.ButtonStartStop.Size = new System.Drawing.Size(127, 23);
            this.ButtonStartStop.TabIndex = 3;
            this.ButtonStartStop.Text = "Start (F9)";
            this.ButtonStartStop.UseVisualStyleBackColor = true;
            this.ButtonStartStop.Click += new System.EventHandler(this.ButtonStartStop_Click);
            // 
            // TimerRun
            // 
            this.TimerRun.Enabled = true;
            this.TimerRun.Interval = 1;
            this.TimerRun.Tick += new System.EventHandler(this.TimerRun_Tick);
            // 
            // Panel1
            // 
            this.Panel1.Controls.Add(this.CheckBoxAutoResize);
            this.Panel1.Controls.Add(this.groupBox1);
            this.Panel1.Controls.Add(this.ButtonStartStop);
            this.Panel1.Controls.Add(this.CheckBoxLooping);
            this.Panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel1.Location = new System.Drawing.Point(1076, 0);
            this.Panel1.Margin = new System.Windows.Forms.Padding(0);
            this.Panel1.Name = "Panel1";
            this.Panel1.Size = new System.Drawing.Size(151, 456);
            this.Panel1.TabIndex = 4;
            // 
            // CheckBoxAutoResize
            // 
            this.CheckBoxAutoResize.AutoSize = true;
            this.CheckBoxAutoResize.Checked = true;
            this.CheckBoxAutoResize.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CheckBoxAutoResize.Location = new System.Drawing.Point(12, 101);
            this.CheckBoxAutoResize.Name = "CheckBoxAutoResize";
            this.CheckBoxAutoResize.Size = new System.Drawing.Size(83, 17);
            this.CheckBoxAutoResize.TabIndex = 5;
            this.CheckBoxAutoResize.Text = "Auto &Resize";
            this.CheckBoxAutoResize.UseVisualStyleBackColor = true;
            this.CheckBoxAutoResize.CheckedChanged += new System.EventHandler(this.CheckBoxAutoResize_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ButtonEnableAll);
            this.groupBox1.Controls.Add(this.ButtonDisableAll);
            this.groupBox1.Controls.Add(this.CheckBoxLiveWindow);
            this.groupBox1.Controls.Add(this.CheckBoxLivePanel2);
            this.groupBox1.Controls.Add(this.CheckBoxLivePanel1);
            this.groupBox1.Location = new System.Drawing.Point(12, 303);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(127, 118);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Live Render";
            // 
            // ButtonEnableAll
            // 
            this.ButtonEnableAll.Location = new System.Drawing.Point(6, 88);
            this.ButtonEnableAll.Name = "ButtonEnableAll";
            this.ButtonEnableAll.Size = new System.Drawing.Size(52, 23);
            this.ButtonEnableAll.TabIndex = 5;
            this.ButtonEnableAll.Text = "&Enable all";
            this.ButtonEnableAll.UseVisualStyleBackColor = true;
            this.ButtonEnableAll.Click += new System.EventHandler(this.ButtonEnableAll_Click);
            // 
            // ButtonDisableAll
            // 
            this.ButtonDisableAll.Location = new System.Drawing.Point(64, 88);
            this.ButtonDisableAll.Name = "ButtonDisableAll";
            this.ButtonDisableAll.Size = new System.Drawing.Size(52, 23);
            this.ButtonDisableAll.TabIndex = 4;
            this.ButtonDisableAll.Text = "&Disable all";
            this.ButtonDisableAll.UseVisualStyleBackColor = true;
            this.ButtonDisableAll.Click += new System.EventHandler(this.ButtonDisableAll_Click);
            // 
            // CheckBoxLiveWindow
            // 
            this.CheckBoxLiveWindow.AutoSize = true;
            this.CheckBoxLiveWindow.Checked = true;
            this.CheckBoxLiveWindow.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CheckBoxLiveWindow.Location = new System.Drawing.Point(6, 65);
            this.CheckBoxLiveWindow.Name = "CheckBoxLiveWindow";
            this.CheckBoxLiveWindow.Size = new System.Drawing.Size(80, 17);
            this.CheckBoxLiveWindow.TabIndex = 3;
            this.CheckBoxLiveWindow.Text = "Window (&3)";
            this.CheckBoxLiveWindow.UseVisualStyleBackColor = true;
            // 
            // CheckBoxLivePanel2
            // 
            this.CheckBoxLivePanel2.AutoSize = true;
            this.CheckBoxLivePanel2.Location = new System.Drawing.Point(6, 42);
            this.CheckBoxLivePanel2.Name = "CheckBoxLivePanel2";
            this.CheckBoxLivePanel2.Size = new System.Drawing.Size(62, 17);
            this.CheckBoxLivePanel2.TabIndex = 2;
            this.CheckBoxLivePanel2.Text = "Panel &2";
            this.CheckBoxLivePanel2.UseVisualStyleBackColor = true;
            // 
            // CheckBoxLivePanel1
            // 
            this.CheckBoxLivePanel1.AutoSize = true;
            this.CheckBoxLivePanel1.Location = new System.Drawing.Point(6, 19);
            this.CheckBoxLivePanel1.Name = "CheckBoxLivePanel1";
            this.CheckBoxLivePanel1.Size = new System.Drawing.Size(62, 17);
            this.CheckBoxLivePanel1.TabIndex = 1;
            this.CheckBoxLivePanel1.Text = "Panel &1";
            this.CheckBoxLivePanel1.UseVisualStyleBackColor = true;
            // 
            // PanelRenderTarget2
            // 
            this.PanelRenderTarget2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(0)))), ((int)(((byte)(153)))));
            this.PanelRenderTarget2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelRenderTarget2.Location = new System.Drawing.Point(538, 0);
            this.PanelRenderTarget2.Margin = new System.Windows.Forms.Padding(0);
            this.PanelRenderTarget2.Name = "PanelRenderTarget2";
            this.PanelRenderTarget2.Size = new System.Drawing.Size(538, 456);
            this.PanelRenderTarget2.TabIndex = 5;
            this.PanelRenderTarget2.Paint += new System.Windows.Forms.PaintEventHandler(this.PanelRenderTarget2_Paint);
            this.PanelRenderTarget2.Resize += new System.EventHandler(this.PanelRenderTarget2_Resize);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel1.Controls.Add(this.Panel1, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.PanelRenderTarget2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.PanelRenderTarget1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1227, 456);
            this.tableLayoutPanel1.TabIndex = 7;
            // 
            // FormMain
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1227, 456);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMain_FormClosing);
            this.Shown += new System.EventHandler(this.FormMain_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormMain_KeyDown);
            this.Panel1.ResumeLayout(false);
            this.Panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel Panel1;
        public System.Windows.Forms.Panel PanelRenderTarget1;
        public System.Windows.Forms.Panel PanelRenderTarget2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.CheckBox CheckBoxLiveWindow;
        public System.Windows.Forms.CheckBox CheckBoxLooping;
        private System.Windows.Forms.Timer TimerRun;
        public System.Windows.Forms.Button ButtonStartStop;
        public System.Windows.Forms.CheckBox CheckBoxLivePanel2;
        public System.Windows.Forms.CheckBox CheckBoxLivePanel1;
        public System.Windows.Forms.Button ButtonDisableAll;
        public System.Windows.Forms.CheckBox CheckBoxAutoResize;
        public System.Windows.Forms.Button ButtonEnableAll;
    }
}

