You must add these files (included in source rar archive) to your .NET application :

   * MessageBox.cs

   * XMessageBox.dll

   * Language resource files (.resx)


==Instructions==


*MessageBox.cs* is the .NET open source code for Managed XMessageBox.

---

*XMessageBox.dll* in the zip file is needed by Managed XMessageBox.

Your application needs to find this dll in its search path. The easiest way is putting the dll to your application's exe folder. Don't forget to use the correct dll version of *x86* or *x64* according to your application target.

This dll was edited and compiled using Hans Dietrich's C++ code which is provided here : http://www.hdsoft.org/xmessagebox.html

Edited dll source code is in the zip file if you want to compile the dll by your self. Don't forget to compile it in *Release* mode. Otherwise you'll get a dll loading error on PCs *not having Visual Studio installed*.

---

*ResourcesButton.resx* is the resource file for button captions.

You must set 2 properties of resx file after you have added it to your project :

   * Custom Tool = PublicResXFileCodeGenerator

   * Custom Tool Namespace = Xtro.Gerecler