﻿/* 
Author : Onur "Xtro" ER
Create date : 11.08.2009
Last update : 13.12.2011
*/

using System;
using System.Runtime.InteropServices;
using System.ComponentModel;

namespace Xtro.Gerecler
{
    [Serializable, StructLayout(LayoutKind.Sequential), ComVisible(true)]
    public struct SSize3D
    {
        public static readonly SSize3D Empty;
        public int Width;
        public int Height;
        public int Depth;

        public SSize3D(int Width, int Height, int Depth)
        {
            this.Width = Width;
            this.Height = Height;
            this.Depth = Depth;
        }

        public static SSize3D operator +(SSize3D Size1, SSize3D Size2)
        {
            return new SSize3D(Size1.Width + Size2.Width, Size1.Height + Size2.Height, Size1.Depth + Size2.Depth);
        }

        public static SSize3D operator -(SSize3D Size1, SSize3D Size2)
        {
            return new SSize3D(Size1.Width - Size2.Width, Size1.Height - Size2.Height, Size1.Depth - Size2.Depth);
        }

        public static bool operator ==(SSize3D Size1, SSize3D Size2)
        {
            return Size1.Width == Size2.Width && Size1.Height == Size2.Height && Size1.Depth == Size2.Depth;
        }

        public static bool operator !=(SSize3D Size1, SSize3D Size2)
        {
            return !(Size1 == Size2);
        }

        [Browsable(false)]
        public bool IsEmpty
        {
            get
            {
                return Width == 0 && Height == 0 && Depth == 0;
            }
        }

        public override bool Equals(object Object)
        {
            if (!(Object is SSize3D)) return false;

            var Size = (SSize3D)Object;
            return Size.Width == Width && Size.Height == Height;
        }

        public override int GetHashCode()
        {
            return Width ^ Height ^ Depth;
        }

        public override string ToString()
        {
            return "{Width=" + Width + ", Height=" + Height + ", Depth=" + Depth + "}";
        }
    }
}