﻿/* 
Author : Onur "Xtro" ER
Create date : 14.05.2009
Last update : 18.06.2010
*/

using System;

namespace Xtro.Gerecler
{
// ReSharper disable InconsistentNaming
    public static class Global
// ReSharper restore InconsistentNaming
    {
        public static readonly Random Random = new Random();
    }
}