﻿/*
Author : Onur "Xtro" ER
Create date : 20.05.2009
Last update : 05.12.2011
*/

using System;
using System.Runtime.InteropServices;
using System.Security;

namespace Xtro.Gerecler
{
// ReSharper disable InconsistentNaming
    public static class Windows
// ReSharper restore InconsistentNaming
    {
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        [SuppressUnmanagedCodeSecurity]
        public static extern IntPtr PostMessage(IntPtr Handle, int Message, int ParameterW, int ParameterL);

        [DllImport("kernel32.dll")]
        [SuppressUnmanagedCodeSecurity]
        public static extern bool QueryPerformanceCounter(out long PerformanceCount);
        [DllImport("kernel32.dll")]
        [SuppressUnmanagedCodeSecurity]
        public static extern void QueryPerformanceFrequency(out long Frequency);

        [DllImport("user32.dll")]
        public static extern bool EnumDisplayDevices(string Device, uint DeviceNumber, ref SDisplayDevice DisplayDevice, uint Flags);

        [Flags]
        public enum EDisplayDeviceStateFlag
        {
            /// <summary>The device is part of the desktop.</summary>
            AttachedToDesktop = 0x1,
            MultiDriver = 0x2,
            /// <summary>The device is part of the desktop.</summary>
            PrimaryDevice = 0x4,
            /// <summary>Represents a pseudo device used to mirror application drawing for remoting or other purposes.</summary>
            MirroringDriver = 0x8,
            /// <summary>The device is VGA compatible.</summary>
            VgaCompatible = 0x16,
            /// <summary>The device is removable; it cannot be the primary display.</summary>
            Removable = 0x20,
            /// <summary>The device has more display modes than its output devices support.</summary>
            ModesPruned = 0x8000000,
            Remote = 0x4000000,
            Disconnect = 0x2000000
        }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public struct SDisplayDevice
        {
            [MarshalAs(UnmanagedType.U4)]
            public int Cb;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
            public string DeviceName;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
            public string DeviceString;
            [MarshalAs(UnmanagedType.U4)]
            public EDisplayDeviceStateFlag StateFlags;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
            public string DeviceID;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
            public string DeviceKey;
        }

        public enum EError
        {
            NoInterface = -2147467262,
            OutOfMemory = -2147024882,
            InvalidArgument = -2147024809,
            Fail = -2147467259,
            Abort = -2147467260,
            False = 1
        };

        public static float TickCount()
        {
            long PerformanceCount;
            QueryPerformanceCounter(out PerformanceCount);
            return PerformanceCount * TickCountPeriod;
        }

        static readonly float TickCountPeriod;

        static Windows()
        {
            long TickCountFrequency;
            QueryPerformanceFrequency(out TickCountFrequency);
            TickCountPeriod = (float)1000 / TickCountFrequency;
        }
    }
}