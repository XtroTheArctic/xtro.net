﻿/*
Author : Onur "Xtro" ER
Create date : 18.12.2011
Last update : 18.12.2011
*/

using System;

using Xtro.DirectX.Direct3D;

using Xtro.Gerecler;

namespace Xtro.DirectX
{
    public sealed class EDirectX : EGerecler
    {
        public EDirectX(string FunctionName, int ErrorCode, string ThrowerName, Exception InnerException = null)
            : base(string.Format(ResourcesError.DirectX, FunctionName), InnerException)
        {
            Init(FunctionName, null, Manager.GetResultText(ErrorCode), ThrowerName);
        }
    }
}
